var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/angtest', function(req, res, next) {
  res.render('app', { title: 'Angular Test'})
});

router.get('/phaser', function(req, res, next) {
  res.render('phaser', { title: 'Phaser Test'})
});

module.exports = router;
