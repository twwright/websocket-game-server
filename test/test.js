var assert = require('chai').assert,
  sinon = require('sinon'),
  gameWss = require('../gameWss');

var sandbox = sinon.sandbox.create(),
  stubbedWs = {
    send: sandbox.stub(),
    on: sandbox.stub()
  },
  gameServer;

describe('GameServer', function() {
  beforeEach(function() {
    gameServer = gameWss();
  });

  describe('onConnection', function() {
    it('should add client to connections', function() {
      gameServer.onConnection(stubbedWs);
      assert.equal(gameServer.connections.length, 1, 'length correct');
      assert.equal(gameServer.connections[0], stubbedWs, 'first connection is new client');
    });

    it('should register websocket events with the client', function() {
      assert.isTrue(stubbedWs.on.calledWith('close'), 'close event registered');
      assert.isTrue(stubbedWs.on.calledWith('message'), 'message event registered');
    });
  });

  describe('onCommand', function() {
    var sandbox;
    before(function() {
      sandbox = sinon.sandbox.create();
    });

    afterEach(function() {
      sandbox.restore();
    });

    it('should call registerGame() once with \'registerGame testGame input1;input2\'', function() {
      sandbox.stub(gameServer, 'registerGame');
      gameServer.onCommand(stubbedWs, "registerGame testGame input1;input2".split(' '));
      assert.isTrue(gameServer.registerGame.calledOnce, '\'registerGame\' called once');
    });

    it('should call disconnectGame() once with \'disconnectGame\'', function() {
      sandbox.stub(gameServer, 'disconnectGame');
      gameServer.onCommand(stubbedWs, "disconnectGame".split(' '));
      assert.isTrue(gameServer.disconnectGame.calledOnce, '\'disconnectGame\' called once');
    });

    it('should call registerClient() once with \'registerClient testClient testGame observer\'', function() {
      sandbox.stub(gameServer, 'registerClient');
      gameServer.onCommand(stubbedWs, "registerClient".split(' '));
      assert.isTrue(gameServer.registerClient.calledOnce, '\'registerClient\' called once');
    });

    it('should call disconnectClient() once with \'disconnectClient\'', function() {
      sandbox.stub(gameServer, 'disconnectClient');
      gameServer.onCommand(stubbedWs, "disconnectClient".split(' '));
      assert.isTrue(gameServer.disconnectClient.calledOnce, '\'disconnectClient\' called once');
    });

    it('should call updateGame() once with \'updateGame theUpdates\'', function() {
      sandbox.stub(gameServer, 'updateGame');
      gameServer.onCommand(stubbedWs, "updateGame theUpdates".split(' '));
      assert.isTrue(gameServer.updateGame.calledOnce, '\'updateGame\' called once');
    });
  });

});
