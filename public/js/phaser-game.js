// defines functions to create the phaser game
decoded = '';
bmd = null;
phaser = null;
sprite = null;
function initPhaser(domId)
{
  phaser = new Phaser.Game(800, 200, Phaser.AUTO, domId, { preload: preload, create: create, update: update, render: render });

  function preload()
  {
    phaser.load.image('background', '/img/back.png');
    bmd = phaser.make.bitmapData(32, 16);
  }

  function create()
  {
    phaser.stage.disableVisibilityChange = true;
    phaser.add.sprite(0, 0, 'background');
    sprite = bmd.addToWorld(0, 0, 0, 0, 8, 8);
    sprite.smoothed = false;
    bmd.rect(16, 4, 2, 2, 'rgba(0,0,255,1)');
  }

  function update()
  {
    if(decoded != '')
    {
      console.log('parsed decoded!');
      for(var y = 0; y < 16; ++y)
      {
        for(var x = 0; x < 32; ++x)
        {
          var r = decoded.charCodeAt(y * 3 * 32 + x * 3);
          var g = decoded.charCodeAt((y * 3 * 32 + x * 3) + 1);
          var b = decoded.charCodeAt((y * 3 * 32 + x * 3) + 2);
          bmd.rect(x, y, 1, 1, 'rgba(' + r + ',' + g + ',' + b + ',1)');
          bmd.dirty = true;
        }
      }
      decoded = '';
    }
  }

  function render()
  {

  }
}

initPhaser('game-container');
