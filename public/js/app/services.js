var services = angular.module('services', []);

services.factory('LogService', function() {
  var service = {
    messages: [],
    count: 0,
    max: 10
  };

  service.add = function(msg) {
    service.count += 1;
    service.messages.push({
      count: service.count,
      message: msg
    });
    // delete excess messages
    if(service.messages.length > service.max) {
      service.messages.splice(0, service.messages.length - service.max);
    }
  }

  service.clear = function() {
    service.messages.splice(0, service.messages.length);
    service.count = 0;
  }

  return service;
});

services.factory('WebSocketService', function() {
  var service = {};


  return service;
});
