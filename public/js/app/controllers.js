var controllers = angular.module('controllers', []);

controllers.controller('ConnectCtrl', ['$rootScope', '$scope', 'LogService', function($rootScope, $scope, LogService) {
  $scope.state = 'DISCONNECTED';
  $scope.logService = LogService;

  $scope.host = 'localhost:8888';
  $scope.gameName = 'game';
  $scope.clientName = 'webclient';
  $scope.clientMode = 'controller';

  $scope.connect = function() {
    $scope.disconnect();
    $scope.ws = new WebSocket('ws://' + $scope.host);
    globalfuck = $scope.ws;
    $scope.ws.onopen = function() {
      $scope.state = 'CONNECTING';
      console.log('Websocket connection established!');
      $scope.logService.add('WebSocket connection established!');
      $scope.ws.send('register ' + $scope.clientName + ' ' + $scope.gameName);
      $scope.logService.add('[>>] register ' + $scope.clientName + ' ' + $scope.gameName);
      $scope.$apply(); // manually apply because update is async
    }
    $scope.ws.onclose = function() {
      $scope.state = 'DISCONNECTED';
      console.log('Websocket disconnected!');
      $scope.logService.add('Websocket disconnected!');
      $rootScope.$emit('ws.close');
      $scope.$apply(); // manually apply because update is async
    }
    $scope.ws.onmessage = function(msg) {
      console.log('[<<] ' + msg.data);
      $scope.logService.add('[<<] ' + (msg.data.length > 80 ? msg.data.substring(0, 80) + '...' : msg.data));
      if($scope.state == 'CONNECTING')
      {
        var split = msg.data.split(' ');
        if(split[0] == 'ok')
        {
          $scope.state = 'CONNECTED';
          console.log('Game connection established! (' + $scope.clientName + ' => ' + $scope.gameName + ', ' + $scope.clientMode + ')');
          $scope.logService.add('Game connection established! (' + $scope.clientName + ' => ' + $scope.gameName + ', ' + $scope.clientMode + ')');
          // do stuff with Phaser if state included
          if(split.length > 1)
            $rootScope.$emit('ws.message', msg.data);
        }
        else {
          // something went wrong, close the Websocket
          $scope.disconnect();
        }
      }
      else {
        $rootScope.$emit('ws.message', msg.data);
      }
      $scope.$apply(); // manually apply because update is async
    };
  };

  $rootScope.$on('phaser.input', function(event, msg) {
    // send the inputs as an update
    var update = {
      input: msg
    };
    var str = 'update ' + JSON.stringify(update);
    $scope.ws.send(str);
    $scope.logService.add('[>>] ' + str);
    $scope.$apply(); // manually apply because update is async
  });
  $scope.disconnect = function() {
    if($scope.ws != null)
    {
      $scope.ws.close();
    }
  };
}]);

controllers.controller('GameCtrl', ['$rootScope', '$scope', 'LogService', function($rootScope, $scope, LogService) {
  $scope.phaser = null;
  $scope.bmd = null;
  $scope.fullscreen = false;
  $scope.upscale = 8;
  $scope.width = null;
  $scope.height = null;
  $scope.inputTime = 0.1;
  $scope.elapsedTime = 0;
  $scope.inputs = {};

  var updateCanvas = function(canvas) {
    // update the bitmap data
    var h = $scope.height;
    var w = $scope.width;
    for(var y = 0; y < h; ++y)
    {
      for(var x = 0; x < w; ++x)
      {
        var r = canvas.charCodeAt(y * 3 * w + x * 3);
        var g = canvas.charCodeAt((y * 3 * w + x * 3) + 1);
        var b = canvas.charCodeAt((y * 3 * w + x * 3) + 2);
        $scope.bmd.rect(x, y, 1, 1, 'rgba(' + r + ',' + g + ',' + b + ',1)');
      }
    }
    $scope.bmd.dirty = true;
  };

  var createPhaser = function(canvasWidth, canvasHeight, bmdWidth, bmdHeight) {
    // create Phaser
    var preload = function() {};
    var create = function() {
      $scope.phaser.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;
      $scope.phaser.stage.disableVisibilityChange = true;
      $scope.bmd = $scope.phaser.make.bitmapData(bmdWidth, bmdHeight);
      var img = $scope.bmd.addToWorld(canvasWidth/2 - bmdWidth*$scope.upscale/2, canvasHeight/2 - bmdHeight*$scope.upscale/2, 0, 0, $scope.upscale, $scope.upscale);
      img.smoothed = false;
    };
    var update = function() {
      // define the key map
      // TODO: define in UI
      var activeInputs = "";
      var keyMap = [
        {
          key: Phaser.KeyCode.W,
          input: "up"
        },
        {
          key: Phaser.KeyCode.S,
          input: "down"
        },
        {
          key: Phaser.KeyCode.A,
          input: "left"
        },
        {
          key: Phaser.KeyCode.D,
          input: "right"
        },
        {
          key: Phaser.KeyCode.ENTER,
          input: "select"
        },
        {
          key: Phaser.KeyCode.ESC,
          input: "quit"
        }
      ];
      // check each input in the input map
      for(var i = 0; i < keyMap.length; ++i) {
        if($scope.phaser.input.keyboard.isDown(keyMap[i].key))
          $scope.inputs[keyMap[i].input] = true;
      }
      // update input timer
      $scope.elapsedTime += ($scope.phaser.time.elapsed / 1000);
      if($scope.elapsedTime >= $scope.inputTime) {
        // reset the timer
        $scope.elapsedTime = 0;
        // create the active input string to send to server
        // clear the inputs as we do this
        var activeInputs = "";
        for (var prop in $scope.inputs) {
          if ($scope.inputs.hasOwnProperty(prop) && $scope.inputs[prop] == true) {
              activeInputs += prop + ';';
              $scope.inputs[prop] = false;
          }
        }
        // send the update if any inputs were detected
        if(activeInputs.length > 1) {
          activeInputs = activeInputs.substring(0, activeInputs.length - 1);
          // emit an event for the inputs
          $rootScope.$emit('phaser.input', activeInputs);
        }
      }
    };
    var render = function() {};
    $scope.phaser = new Phaser.Game(canvasWidth, canvasHeight, Phaser.AUTO, "game-container", { preload: preload, create: create, update: update, render: render });
    //$scope.bmd.dirty = true;
    $scope.$apply();
  }

  var destroyPhaser = function() {
    LogService.add('Cleaning up Phaser.io...');
    $scope.phaser.destroy();
    $scope.phaser = null;
    $scope.width = null;
    $scope.height = null;
    $scope.$apply();
  }

  $rootScope.$on('ws.message', function(event, msg) {
    // retrieve canvas data from WebSocket message
    var split = msg.split(' ');
    if(split.length > 1 && (split[0] == 'update' || split[0] == 'ok')) {
      var state = JSON.parse(split[1]);
      if(state.width > 0 && state.height > 0 && (state.width != $scope.width || state.height != $scope.height)) {
        if($scope.phaser != null)
          destroyPhaser();
        LogService.add('Starting Phaser.io!');
        createPhaser(800, 450, state.width, state.height);
        $scope.width = state.width;
        $scope.height = state.height;
      }
      // parse the provided inputs
      if(typeof(state.inputs) !== 'undefined') {
        var inputs = state.inputs.split(';');
        for(var i = 0; i < inputs.length; ++i)
          $scope.inputs[inputs[i]] = false;
      }
      // parse the canvas
      if(typeof(state.canvas) !== 'undefined' && $scope.bmd != null) {
        var canvas = atob(state.canvas);
        updateCanvas(canvas);
      }
      $scope.$apply();
    }
  });

  $rootScope.$on('ws.close', function(event) {
    if($scope.phaser)
    {
      destroyPhaser();
    }
  });

  $scope.fullscreen = function() {
    if($scope.phaser.scale.isFullscreen)
    {
      $scope.phaser.scale.stopFullScreen();
    }
    else
    {
      $scope.phaser.scale.startFullScreen(false);
    }
  }
}]);

controllers.controller('LogCtrl', ['$scope', 'LogService', function($scope, LogService) {
  $scope.logService = LogService;
  $scope.clear = function() {
    // empty the messages array
    LogService.clear();
  };

  $scope.add = function() {
    // add a message
    LogService.add('Test Message!');
  }
}]);
