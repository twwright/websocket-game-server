
module.exports = function()
{
  var self = {}
  self.total = 0
  self.connections = [];
  self.games = [];
  self.debug = true;

  self.onConnection = function(ws) {
    self.connections.push(ws);
    console.log('Client connected! (' + self.connections.length + ')');

    ws.on('close', function() {
      --self.total;
      for(var i = 0; i < self.connections.length; ++i)
      {
        if(self.connections[i] == ws)
        {
          self.connections.splice(i, 1);
          break;
        }
      }
      console.log('Client disconnected! (' + self.connections.length + ')');
      // deal with disconnection
      if(ws.game != null)
      {
        self.onCommand(ws, ['disconnect']);
      }
    });

    ws.on('message', function(msg) {
      var split = msg.split(' ');
      // splice out any elements that are empty string ''
      for(var i = split.length - 1; i >= 0; --i)
      {
        if(split[i].length == 0)
          split.splice(i, 1);
      }
      self.onCommand(ws, split);
    })

    ws.ok = function(msg) {
      ws.send('ok ' + msg);
    }

    ws.error = function(msg) {
      ws.send('error ' + msg);
    }
  };

  self.onCommand = function(ws, args)
  {
    try
    {
      var command = args[0], msg = null;
      switch(command)
      {
        case 'echo':
          ws.send(args[1]);
          break;
        case 'register':
          msg = self.register(ws, args[1], args[2]);
          break;
        case 'disconnect':
          msg = self.disconnect(ws);
          break;
        case 'update':
          msg = self.update(ws, args[1]);
          break;
        case 'queryGames':
          var resp = "Games: ";
          for(var i = 0; i < self.games.length; ++i)
          {
            resp += '[' + i + ' | ' + self.games[i].name + ' : ' + self.games[i].observers.length + '] ';
          }
          ws.send(resp);
          break;
        case 'queryClients':
          var resp = "Clients: ";
          for(var i = 0; i < self.connections.length; ++i)
          {
            resp += '[' + i + ' | ' + self.connections[i].name + ' : ';
            resp += ((self.connections[i].game != null) ? self.connections[i].game.name : '---') + '] ';
          }
          ws.send(resp);
          break;
        default:
          throw new Error('unknown command \'' + command + '\'');
      }
      if(ws.readyState == ws.OPEN)
        ws.send('ok' + (msg == null ? '' : ' ' + msg));
    }
    catch(ex)
    {
      if(ws.readyState == ws.OPEN)
        ws.send('error [' + ex.name + '] ' + ex.message);
      console.log('error [' + ex.name + '] ' + ex.message);
      console.log(ex.stack);
    }
  };

  // COMMAND CALLBACKS

  self.register = function(ws, name, gameName) {
    if(ws.game != null)
    {
      throw new Error('client already registered in \'' + ws.game.name + '\'');
    }
    var game = self.findGame(gameName);
    if(game == null)
    {
      game = self.createGame(gameName);
    }
    game.observers.push(ws);
    ws.game = game;
    ws.name = name;
    return JSON.stringify(game.state);
  }

  self.disconnect = function(ws) {
    // check for a game registered on ws
    if(ws.game == null)
    {
      throw new Error('no game registered');
    }

    // remove the client from the game and notify the others of the disconnect
    var clientIndex = -1;
    for(var i = 0; i < ws.game.observers.length; ++i)
    {
      if(ws.game.observers[i] == ws)
        clientIndex = i;
      else
        ws.game.observers[i].send('disconnectedClient ' + ws.name);
    }
    if(clientIndex >= 0)
      ws.game.observers.splice(clientIndex, 1);

    // delete the game if no-one is in it
    if(ws.game.observers.length == 0 && ws.game.gameWs == null) {
      self.deleteGame(ws.game);
    }

    // remove the client's game and name
    ws.game = null;
    ws.name = null;
  }

  self.update = function(ws, stateUpdates)
  {
    if(ws.game == null)
    {
      throw new Error('no game registered');
    }
    // apply updates to the current game state
    stateUpdates = JSON.parse(stateUpdates);
    for (var prop in stateUpdates) {
      if (stateUpdates.hasOwnProperty(prop)) {
          ws.game.state[prop] = stateUpdates[prop];
      }
    }
    /*
    // parse updates
    for(var i = 0; i < updates.length; ++i)
    {
      var split = updates[i].split('\=');
      //console.log(i + ': ' + split);
      if(split.length < 2 || split[0].length == 0 || split[1].length == 0)
        throw new Error('bad syntax for game update');
      ws.game.state[split[0]] = split[1];
    }
    */
    console.log("Updating game '" + ws.game.name + "' by '" + ws.name + "'...");
    // notify the OTHER observers of the update
    for(var i = 0; i < ws.game.observers.length; ++i)
    {
      if(ws.game.observers[i] != ws)
        ws.game.observers[i].send('update ' + JSON.stringify(stateUpdates));
    }
  };

  // HELPER functions

  self.createGame = function(name) {
    console.log("Creating new game '" + name + "'...");
    var game = {
      name: name,
      gameWs: null,
      controller: null,
      observers: [],
      state: {
      }
    };
    self.games.push(game);
    return game;
  }

  self.deleteGame = function(game) {
    // iterate games and remove the game
    var debugStr = "Deleting game '" + game.name + "'... ";
    for(var i = 0; i < self.games.length; ++i)
    {
      if(game == self.games[i])
      {
        self.games.splice(i, 1);
        console.log(debugStr + ' Deleted!');
        return true;
      }
    }
    console.log(debugStr + ' Not found');
    return false;
  }

  self.findGame = function(name)
  {
    for(var i = 0; i < self.games.length; ++i)
    {
      if(self.games[i].name == name)
        return self.games[i];
    }
    return null;
  };

  return self;
}
